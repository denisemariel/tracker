class CreateTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.string :task
      t.string :note
      t.string :date

      t.timestamps
    end
  end
end
