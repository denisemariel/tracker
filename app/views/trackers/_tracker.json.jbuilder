json.extract! tracker, :id, :task, :note, :date, :created_at, :updated_at
json.url tracker_url(tracker, format: :json)
